namespace $.$$ {

	export class ctx {
		@$mol_mem
		static words( val?: any ) {
			if( val !== undefined ) return val as string
			return ""
		}
	}

	export class $kinsle extends $.$kinsle {

		@$mol_mem
		kinsle_tab_report() {
			return new $kinsle_tab_report
		}

		@$mol_mem
		kinsle_tab_export() {
			return new $kinsle_tab_export
		}

		@$mol_mem
		kinsle_tab_template() {
			return new $kinsle_tab_template
		}

		@$mol_mem
		content( route?: string ) {
			switch( route ) {
				case "reports":
					return this.kinsle_tab_report()
				case "exports":
					return this.kinsle_tab_export()
				case "templates":
					return this.kinsle_tab_template()
			}
		}

		@$mol_mem
		event_route( val?: any ) {
			const id = val.currentTarget.id
			const route = eval( id ).route().trim()
			this.content( route )
		}
	}


	interface kinsle_tab_export {
		id: string
		title: string
		size: number
		date: number
	}
	export class $kinsle_tab_export extends $.$kinsle_tab_export {

		@$mol_mem
		search( val?: any ) {
			if( val !== undefined ) return val as never
			return ""
		}

		@$mol_mem
		rows() {
			return [
				{ id: "1", title: "first file", size: 14, date: 12 },
				{ id: "2", title: "second file", size: 423, date: 12 },
				{ id: "3", title: "third file", size: 1, date: 12 },
				{ id: "4", title: "fucker file", size: 17, date: 12 },
			] as kinsle_tab_export[]
		}

		@$mol_mem
		row_size( id: any ) {
			const index = this.rows().indexOf( id )
			const item = this.rows()[ index ]
			if( item !== undefined ) {
				return item.size
			}
			return 0
		}
		@$mol_mem
		row_title( id: any ) {
			const index = this.rows().indexOf( id )
			const item = this.rows()[ index ]
			if( item !== undefined ) {
				return item.title
			}
			return 0
		}

		@$mol_mem
		sub() {
			let rows = this.rows()
			if( ctx.words().trim() !== "" ) {
				rows = this.rows().filter( e => e.title.includes( ctx.words() ) )
			}

			let out = []
			for( let index = 0; index < this.rows().length; index++ ) {
				const item = rows[ index ]
				if( item !== null )
					out.push( this.Row( item ) )
			}
			return out || []
		}
	}

	export class $kinsle_search extends $.$kinsle_search {
		@$mol_mem
		word( val?: any ) {
			if( val !== undefined ) {
				ctx.words( val )
				return val as string
			}
			return ""
		}
	}
}
