namespace $.$$ {
	export class $kinsle_auth extends $.$kinsle_auth {
		async login_button() {
			try {
				const response = await fetch( 'https://web.kinsle.ru/account/login', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json;charset=utf-8'
					},
					body: JSON.stringify( { login: this.Login_string().value(), password: this.Password_string().value() } )
				} )


				if( response.status === 403 ) {
					throw new Error( `Wrong username or password` )
				} else if( response.status === 400 ) {
					
				}

				const result = await response.json()

				if(result[0] === 'LoginNotFound'){
					alert( "Пользователь не найден!" )
				} else{
					console.log(result)
				//$mol_state_local.value("accessToken", result.apiKey[0].key);
				}
				$mol_state_local.value( 'token', "ss" )
			} catch( error ) {
				$mol_state_local.value( 'token', null )
				alert( error )
			}
		}
		async register_button() {
			try {
				const response = await fetch( 'https://web.kinsle.ru/account/register', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json;charset=utf-8'
					},
					body: JSON.stringify( { login: this.Login_string().value(), password: this.Password_string().value() } )
				} )

				if( response.status === 403 ) {
					throw new Error( `Wrong username or password` )
				} else if( (response.status === 200)) {
					const result = await response.json()
					//$mol_state_local.value("accessToken", result.apiKey[0].key);
					console.log( result )
					alert( result )
				} else {
					throw new Error( response.statusText )
				}

			} catch( error ) {
				$mol_state_local.value( 'token', null )
				alert( error )
			}
		}
	}
}
